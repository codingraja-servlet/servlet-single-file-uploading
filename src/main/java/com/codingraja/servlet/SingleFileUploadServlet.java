package com.codingraja.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@MultipartConfig
@WebServlet("/SingleFileUploadServlet")
public class SingleFileUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SingleFileUploadServlet() {
		// Do Nothing
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("index.html").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		// Getting File data
		Part part = request.getPart("fileData");

		// Getting Application Path
		String appPath = request.getServletContext().getRealPath("");

		String message = "File has been uploaded successfully!";

		// Writing File Data
		try {
			part.write(appPath + part.getSubmittedFileName());
		} catch (Exception ex) {
			message = "Exception: " + ex.getMessage();
		}

		out.println("<h1 style=\"text-align:center;\">" + message + "</h1>");
	}
}
